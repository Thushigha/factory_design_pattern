package bcas.ap.dp.fact;

import Interface.Rectangle;

public class ShapeFactoryDemo {
	
		public static void main(String[] args) {
			
		ShapeFactory shapefactory=new ShapeFactory();
		
		
		Shape circle = shapefactory.callShape(ShapeType.CIRCLE);
		circle.draw();
		Circle cir = new Circle();
		System.out.println("Circle area is " + cir.getareaOfCircle(14) + ". Circle perimeter is " + cir.getperimeterOfCircle(14));
		
		
		
		Shape rectangle = shapefactory.callShape(ShapeType.RECTANGLE);
		rectangle.draw();
		Rectangle rec = new Rectangle();
		System.out.println("Rectangle area is " + rec.getArea(10,20) + ". Rectangle perimeter is " + rec.getPerimeter(10, 20));
		
		
		
		Shape square = shapefactory.callShape(ShapeType.SQUARE);
		square.draw();
		Square sqr = new Square();
		System.out.println("Square area is " + sqr.getArea(10,10) + ". Square perimeter is " + sqr.getPerimeter(10));
		
		
		
		Shape pentagon = shapefactory.callShape(ShapeType.PENTAGON);
		pentagon.draw();
		Pentagon pen = new Pentagon();
		System.out.println("Pentagon area is " + pen.getareaOfPentagon(6,12) + ". Pentagon perimeter is " + pen.getperimeterOfPentagon(6));
		
		
	}
}

